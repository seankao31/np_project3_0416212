#include <algorithm>
#include <cctype>
#include <string>
#include <vector>
#include <unistd.h>

#include "utility.h"

using namespace std;

string replace_all(string str, const string& from, const string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}

// trim from start (in place)
string ltrim(const string &str) {
    string s = str;
    s.erase(s.begin(), find_if(s.begin(), s.end(), [](int ch) {
        return !isspace(ch);
    }));
    return s;
}

// trim from end (in place)
string rtrim(const string &str) {
    string s = str;
    s.erase(find_if(s.rbegin(), s.rend(), [](int ch) {
        return !isspace(ch);
    }).base(), s.end());
    return s;
}

// trim from both ends (in place)
string trim(const string &str) {
    string s = str;
    s = ltrim(s);
    s = rtrim(s);
    return s;
}

vector<string> split(string s, const string &delimiter) {
    size_t pos = 0;
    vector<string> words;
    while ((pos = s.find(delimiter)) != string::npos) {
        words.emplace_back(s.substr(0, pos));
        s.erase(0, pos + delimiter.length());
    }
    words.emplace_back(s);
    return words;
}

int myreadline(int sockfd, std::string &str) {
    char cstr[MAXLINE];
    if (_readline(sockfd, cstr, MAXLINE) == 0) {
        str = std::string(cstr);
        return 0;
    }
    return -1;
}

int _readline(int sockfd, char *vptr, size_t maxlen) {
    char *ptr = vptr;
    char c;
    ssize_t rc, n;
    for (n = 1; n < maxlen; n++) {
        if ((rc = bufread(sockfd, &c)) == 1) {
            *ptr++ = c;
            if (c == '\n')
                break;
        }
        else if (rc == 0) {
            break;
        }
        else
            return -1;
    }
    *ptr = '\0';
    return 0;
}

int bufread(int sockfd, char *ptr) {
    static int read_cnt = 0;
    static char *read_ptr;
    static char read_buf[MAXLINE];

    while (read_cnt <= 0) {
        if ((read_cnt = read(sockfd, read_buf, sizeof(read_buf))) < 0) {
            if (errno == EINTR)
                continue;
            return -1;
        }
        else if (read_cnt == 0)
            return 0;
        read_ptr = read_buf;
    }
    read_cnt--;
    *ptr = *read_ptr++;
    return 1;
}

ssize_t readn(int sockfd, char *vptr, size_t n) {
    size_t nleft;
    ssize_t nread;
    char *ptr;

    ptr = vptr;
    nleft = n;
    while (nleft > 0) {
        // errorlog("reading...");
        // errorlog(ptr);
        if ((nread = read(sockfd, ptr, nleft)) < 0) {
            if (errno == EINTR) {
                nread = 0;
            }
            else if (errno == EAGAIN || errno == EWOULDBLOCK) {
                return n - nleft;
            }
            else {
                return -1;
            }
        }
        else if (nread == 0)
            break;

        nleft -= nread;
        ptr += nread;
    }
    return n - nleft;
}

ssize_t writen(int sockfd, const char *vptr, size_t n) {
    size_t left = n;
    ssize_t written;
    const char *ptr = vptr;

    while (left > 0) {
        if ((written = write(sockfd, ptr, left)) <= 0) {
            if (written < 0 && errno == EINTR) {
                written = 0;
            }
            else {
                return -1;
            }
        }

        left -= written;
        ptr += written;
    }
    return n;
}
