#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <string>
#include <unordered_map>

#include "utility.h"

class RequestHandler {
public:
    int sockfd;
    std::string requestline;
    std::string method;
    std::string path;
    std::string request_version;
    std::unordered_map<std::string, std::string> headers;

public:
    RequestHandler(int fd);
    RequestHandler();
    ~RequestHandler();

    bool parse_request();
    void read_headers();
    void handle_request();
    void send_response(int status);
    void clear_headers();
    void add_header(const std::string &keyword, const std::string &value);
    void send_headers();
    int do_GET();
};
