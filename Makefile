CGI_OBJ = hw3_cgi.o utility.o
HTTP_OBJ = http_server.o request_handler.cpp utility.o
DEPS = request_handler.h utility.h
FLAGS = -std=c++11

all: hw3.cgi http_server
	cp hw3.cgi myhttpserver/
	mv hw3.cgi ~/public_html/
	mv http_server myhttpserver/

hw3.cgi: $(CGI_OBJ)
	g++ -o $@ $+ $(FLAGS) -static

http_server: $(HTTP_OBJ)
	g++ -o $@ $+ $(FLAGS)

%.o: %.cpp $(DEPS)
	g++ -c -o $@ $< $(FLAGS)

clean:
	rm -f hw3.cgi http_server *.o
