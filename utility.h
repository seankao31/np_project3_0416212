#include <string>
#include <vector>

#define MAXLINE 10001

int bufread(int sockfd, char *ptr);
int _readline(int sockfd, char *vptr, size_t maxlen);
int myreadline(int sockfd, std::string &str);
ssize_t readn(int, char*, size_t);
ssize_t writen(int, const char*, size_t);

std::string replace_all(std::string, const std::string&, const std::string&);

// trim from start (in place)
std::string ltrim(const std::string &s);

// trim from end (in place)
std::string rtrim(const std::string &s);

// trim from both ends (in place)
std::string trim(const std::string &s);

std::vector<std::string> split(std::string s, const std::string &delimiter);
