#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>

#include "utility.h"
#include "request_handler.h"

using namespace std;

RequestHandler::RequestHandler(int fd): sockfd(fd) {}
RequestHandler::RequestHandler() {}
RequestHandler::~RequestHandler() {}

bool RequestHandler::parse_request() {
    method = "";
    request_version = "HTTP/1.1";
    // rstrip ('\r\n')
    requestline = rtrim(requestline);
    vector<string> words = split(requestline, " ");
    if (words.size() != 3) {
        return false;
    }
    method = words[0];
    path = words[1];
    path = path.substr(1);
    int pos;
    if ((pos = path.find("?")) != string::npos) {
        setenv("QUERY_STRING", path.substr(pos).c_str(), 1);
        path = path.substr(0, pos);
    }
    cerr << "path: " << path << endl;
    request_version = words[2];
    return true;
}

void RequestHandler::read_headers() {
    clear_headers();
    string line;
    while (true) {
        myreadline(sockfd, line);
        if (trim(line).empty())
            break;
        vector<string> words = split(line, ":");
        add_header(trim(words[0]), trim(words[1]));
    }
}

void RequestHandler::handle_request() {
    myreadline(sockfd, requestline);
    read_headers();
    // TODO: do something with headers
    if (!parse_request()) {
        cerr << "Parse Request Error: " << requestline << endl;
        send_response(400);
        // generate bad request html
        return;
    }
    clear_headers();
    send_response(200);
    if (method == "GET") {
        do_GET();
    }
}

int RequestHandler::do_GET() {
    size_t pos = path.find(".cgi");
    if (pos == string::npos || pos != path.length()-4) {
        // forward data
        int filefd = open(path.c_str(), O_RDONLY);
        // TODO: check file open correct

        add_header("Content-Type", "text/html");
        send_headers();

        struct stat stat_buf;
        fstat(filefd, &stat_buf);
        char readbuf[stat_buf.st_size];
        int len;
        while ((len = readn(filefd, readbuf, stat_buf.st_size)) > 0) {
            readbuf[len] = '\0';
            writen(sockfd, readbuf, sizeof(readbuf));
        }
        return 0;
    }
    else {
        // cgi
        setenv("CONTENT_LENGTH", to_string(strlen(getenv("QUERY_STRING"))).c_str(), 1);
        setenv("REQUEST_METHOD", "GET", 1);
        setenv("SCRIPT_NAME", path.c_str(), 1);
        setenv("REMOTE_HOST", "host", 1);
        setenv("REMOTE_ADDR", "addr", 1);
        setenv("AUTH_TYPE", "http", 1);
        setenv("REMOTE_USER", "alpha", 1);
        setenv("REMOTE_IDENT", "beta", 1);
        dup2(sockfd, 0);
        dup2(sockfd, 1);
        execl(path.c_str(), NULL);
    }
}

void RequestHandler::send_response(int status) {
    string response_message = "HTTP/1.1 " + to_string(status) + " ";
    switch(status) {
        case 400:
            response_message += "Bad Request";
            break;
        case 200:
            response_message += "OK";
            break;
    }
    response_message += "\r\n";
    writen(sockfd, response_message.c_str(), response_message.length());
}

void RequestHandler::clear_headers() {
    headers.clear();
}

void RequestHandler::add_header(const string &keyword, const string &value) {
    headers[keyword] = value;
}

void RequestHandler::send_headers() {
    for (auto header: headers) {
        string msg = header.first + ": " + header.second + "\r\n";
        writen(sockfd, msg.c_str(), msg.length());
    }
    writen(sockfd, "\r\n", 2);
}

